input = ["#..#....#...#.#..#.......##.#.####", "#......#..#.#..####.....#..#...##.", ".##.......#..#.#....#.#..#.#....#.", "###..#.....###.#....##.....#...#..", "...#.##..#.###.......#....#....###", ".####...##...........##..#..#.##..", "..#...#.#.#.###....#.#...##.....#.", "......#.....#..#...##.#..##.#..###", "...###.#....#..##.#.#.#....#...###", "..#.###.####..###.#.##..#.##.###..", "...##...#.#..##.#............##.##", "....#.##.##.##..#......##.........", ".#..#.#..#.##......##...#.#.#...##", ".##.....#.#.##...#.#.#...#..###...", "#.#.#..##......#...#...#.......#..", "#.......#..#####.###.#..#..#.#.#..", ".#......##......##...#..#..#..###.", "#.#...#..#....##.#....#.##.#....#.", "....#..#....##..#...##..#..#.#.##.", "#.#.#.#.##.#.#..###.......#....###", "...#.#..##....###.####.#..#.#..#..", "#....##..#...##.#.#.........##.#..", ".#....#.#...#.#.........#..#......", "...#..###...#...#.#.#...#.#..##.##", ".####.##.#..#.#.#.#...#.##......#.", ".##....##..#.#.#.......#.....####.", "#.##.##....#...#..#.#..###..#.###.", "...###.#..#.....#.#.#.#....#....#.", "......#...#.........##....#....##.", ".....#.....#..#.##.#.###.#..##....", ".#.....#.#.....#####.....##..#....", ".####.##...#.......####..#....##..", ".#.#.......#......#.##..##.#.#..##", "......##.....##...##.##...##......"];

// input = [".#..#", ".....", "#####", "....#", "...##"];
// input = ["......#.#.", "#..#.#....", "..#######.", ".#.#.###..", ".#..#.....", "..#....#.#", "#..#....#.", ".##.#..###", "##...#..#.", ".#....####"];
// input = ["#.#...#.#.", ".###....#.", ".#....#...", "##.#.#.#.#", "....#.#.#.", ".##..###.#", "..#...##..", "..##....##", "......#...", ".####.###."];
// input = [".#..#..###", "####.###.#", "....###.#.", "..###.##.#", "##.##.#.#.", "....###..#", "..#.#..#.#", "#..#.#.###", ".##...##.#", ".....#.#.."];
// input = [".#..##.###...#######", "##.############..##.", ".#.######.########.#", ".###.#######.####.#.", "#####.##.#.##.###.##", "..#####..#.#########", "####################", "#.####....###.#.#.##", "##.#################", "#####.##.###..####..", "..######..##.#######", "####.##.####...##..#", ".#####..#.######.###", "##...#.##########...", "#.##########.#######", ".####.#.###.###.#.##", "....##.##.###..#####", ".#.#.###########.###", "#.#.#.#####.####.###", "###.##.####.##.#..##"];

runIt = input => {
	const asteroidMap = [];
	const asteroidPoints = {};
	const asteroidArr = [];
	for(let row = 0; row < input.length; row++) {
		asteroidMap[row] = [];
		for(let col = 0; col < input[row].length; col++) {
			const pointHasAsteroid = input[row][col] === "#";
			asteroidMap[row][col] = pointHasAsteroid;
			if(pointHasAsteroid) {
				asteroidPoints[`${col},${row}`] = true;
				asteroidArr.push([col, row]);
			}
		}
	}

	const linePoints = (a, b) => {
		const [ax, ay] = a;
		const [bx, by] = b;
		const xDiff = Math.abs(ax - bx);
		const yDiff = Math.abs(ay - by);

		const points = [];

		if(yDiff === 0) {
			for(let i = 1; i < xDiff; i++) {
				points.push([ax + (ax > bx ? -i : i), ay]);
			}
		} else if (xDiff === 0) {
			for(let i = 1; i < yDiff; i++) {
				points.push([ax, ay + (ay > by ? -i : i)]);
			}
		} else {
			const stepsCount = Math.max(xDiff, yDiff);

			const xStep = xDiff / stepsCount;
			const yStep = yDiff / stepsCount;

			for(let x = xStep, y = yStep, stepCounter = 1;
				stepCounter < stepsCount;
				stepCounter++, x += xStep, y +=yStep) {
				points.push([ax + parseFloat((ax > bx ? -x : x).toFixed(10)), ay + parseFloat((ay > by ? -y : y).toFixed(10))]);
			}

		}

		return [a, ...points, b];
	};

	const pointIsEqual = (a, b) => {
		return a[0] === b[0] && a[1] === b[1];
	};

	// is B visible from A?:
	// get all points from a to b (on a grid)
	// does any of the points intersect with some asteroid?
	const canSeeEachOther = (a, b) => {
		const crossPoints = linePoints(a, b);

		const asteroidsInTheWay = crossPoints.filter((point) => {
			const [x, y] = point;
			return asteroidPoints[`${x},${y}`] !== undefined
				&& !pointIsEqual(a, point)
				&& !pointIsEqual(b, point);
			}
		);

		// console.log(asteroidsInTheWay);
		return asteroidsInTheWay.length === 0;
	};

	// get number of visible asteroids from the point A:
	// iterate all the points
	// 	point B
	// is point b visible from A?
	const visibleAsteroidsFromPoint = point => {
		const allOtherAsteroids = asteroidArr.filter(astPoint => !pointIsEqual(astPoint, point));
		return [allOtherAsteroids.filter(astPoint => canSeeEachOther(point, astPoint)).length, point];
	};

	// iterate all asteroids
	// 	get number of visible asteroids from that point
	const visibleAsteroidsCount = asteroidArr.map(visibleAsteroidsFromPoint).sort((a, b) => b[0] - a[0]);
	console.log(visibleAsteroidsCount)
	// 334
	return visibleAsteroidsCount[0];
};

runIt(input);
