const input = ["#..#....#...#.#..#.......##.#.####", "#......#..#.#..####.....#..#...##.", ".##.......#..#.#....#.#..#.#....#.", "###..#.....###.#....##.....#...#..", "...#.##..#.###.......#....#....###", ".####...##...........##..#..#.##..", "..#...#.#.#.###....#.#...##.....#.", "......#.....#..#...##.#..##.#..###", "...###.#....#..##.#.#.#....#...###", "..#.###.####..###.#.##..#.##.###..", "...##...#.#..##.#............##.##", "....#.##.##.##..#......##.........", ".#..#.#..#.##......##...#.#.#...##", ".##.....#.#.##...#.#.#...#..###...", "#.#.#..##......#...#...#.......#..", "#.......#..#####.###.#..#..#.#.#..", ".#......##......##...#..#..#..###.", "#.#...#..#....##.#....#.##.#....#.", "....#..#....##..#...##..#..#.#.##.", "#.#.#.#.##.#.#..###.......#....###", "...#.#..##....###.####.#..#.#..#..", "#....##..#...##.#.#.........##.#..", ".#....#.#...#.#.........#..#......", "...#..###...#...#.#.#...#.#..##.##", ".####.##.#..#.#.#.#...#.##......#.", ".##....##..#.#.#.......#.....####.", "#.##.##....#...#..#.#..###..#.###.", "...###.#..#.....#.#.#.#....#....#.", "......#...#.........##....#....##.", ".....#.....#..#.##.#.###.#..##....", ".#.....#.#.....#####.....##..#....", ".####.##...#.......####..#....##..", ".#.#.......#......#.##..##.#.#..##", "......##.....##...##.##...##......"]
// const input = [".#..##.###...#######", "##.############..##.", ".#.######.########.#", ".###.#######.####.#.", "#####.##.#.##.###.##", "..#####..#.#########", "####################", "#.####....###.#.#.##", "##.#################", "#####.##.###..####..", "..######..##.#######", "####.##.####...##..#", ".#####..#.######.###", "##...#.##########...", "#.##########.#######", ".####.#.###.###.#.##", "....##.##.###..#####", ".#.#.###########.###", "#.#.#.#####.####.###", "###.##.####.##.#..##"];

function runIt(input) {
	const asteroidMap = [];
	const asteroidArr = [];

	for(let row = 0; row < input.length; row++) {
		asteroidMap[row] = [];
		for(let col = 0; col < input[row].length; col++) {
			const pointHasAsteroid = input[row][col] === "#";
			asteroidMap[row][col] = pointHasAsteroid;
			if(pointHasAsteroid) {
				asteroidArr.push([col, row]);
			}
		}
	}

	const linePoints = (a, b) => {
		const [ax, ay] = a;
		const [bx, by] = b;
		const xDiff = Math.abs(ax - bx);
		const yDiff = Math.abs(ay - by);

		const points = [];

		if(yDiff === 0) {
			for(let i = 1; i < xDiff; i++) {
				points.push([ax + (ax > bx ? -i : i), ay]);
			}
		} else if (xDiff === 0) {
			for(let i = 1; i < yDiff; i++) {
				points.push([ax, ay + (ay > by ? -i : i)]);
			}
		} else {
			const stepsCount = Math.max(xDiff, yDiff);

			const xStep = xDiff / stepsCount;
			const yStep = yDiff / stepsCount;

			for(let x = xStep, y = yStep, stepCounter = 1;
				stepCounter < stepsCount;
				stepCounter++, x += xStep, y +=yStep) {
				points.push([ax + parseFloat((ax > bx ? -x : x).toFixed(10)), ay + parseFloat((ay > by ? -y : y).toFixed(10))]);
			}

		}

		return [a, ...points, b];
	};

	const pointIsEqual = (a, b) => {
		return a[0] === b[0] && a[1] === b[1];
	};

	const getCoordinatesAndDist = (from, to) => {
		const [ax, ay] = from;
		const [bx, by] = to;
		const x = (ax > bx ? -1 : 1) * Math.abs(ax - bx);
		const y = (ay > by ? 1 : -1) * Math.abs(ay - by);
		const dist = Math.sqrt(x ** 2 + y ** 2);
		return [x, y, dist];
	};

	const toNaturalDeg = angleInRad => {
		const angleInDeg = 180 / Math.PI * angleInRad;
		if(angleInDeg < 0) {
			return 360 + angleInDeg;
		}
		return angleInDeg;
	};

	const monitoringStationPos = [23, 20];

	const asteroidAngleAndDist = asteroidArr
		.filter(astPoint => !pointIsEqual(astPoint, monitoringStationPos))
		.map(ast => [getCoordinatesAndDist(monitoringStationPos, ast), ast])
		.map(([[x, y, dist], ast]) => [toNaturalDeg(Math.atan2(y, x)), dist, ast])
		.sort(([aAngle, aDist], [bAngle, bDist]) => {
			const diff = aAngle - bAngle;
			return diff === 0
				? aDist - bDist
				: diff;
		});

	const findClosestAsteroid = (angle, asteroids) => {
		const nextInLine = asteroids
			.filter(([astAngle]) => astAngle <= angle)
			.sort(([aAngle, aDist], [bAngle, bDist]) => {
				const diff = bAngle - aAngle;
				return diff === 0
					? aDist - bDist
					: diff;
			});
		if(nextInLine.length === 0 && asteroids.length) {
			return findClosestAsteroid(360, asteroids);
		}
		return nextInLine[0];
	};

	let laserMoves = 1;
	let lastVaporizedAngle = 90.0000000001;

	while(true && asteroidAngleAndDist.length > 0) {
		let ast = findClosestAsteroid(lastVaporizedAngle, asteroidAngleAndDist);
		if(ast[0] === lastVaporizedAngle) {
			console.log(`skip ${ast[2]}; angle: ${ast[0]}; dist: ${ast[1]}`)
			ast = findClosestAsteroid(lastVaporizedAngle - 1e-10, asteroidAngleAndDist);
		}

		asteroidAngleAndDist.splice(asteroidAngleAndDist.indexOf(ast), 1);
		lastVaporizedAngle = ast[0];

		console.log(`vaporize ${ast[2]}; angle: ${ast[0]}; dist: ${ast[1]}`);

		if(laserMoves === 200) {
			// 11, 19
			console.log(`200th asteroid: ${ast[2]}; angle: ${ast[0]}; dist: ${ast[1]}`)
			return;
		}
		laserMoves++;
	}
};

runIt(input);

// -1107 is not the right answer
// 9011 too high
// 1213 too high
// That's not the right answer. Curiously, it's the right answer for someone else; you might be logged in to the wrong account or just unlucky. In any case, you need to be using your puzzle input. If you're stuck, make sure you're using the full input data; there are also some general tips on the about page, or you can ask for hints on the subreddit. Because you have guessed incorrectly 5 times on this puzzle, please wait 5 minutes before trying again. (You guessed 408.)