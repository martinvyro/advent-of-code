input = "359282-820401";

doIt = input => {
	const range = input.split("-").map(i => parseInt(i, 10));

	const isValid = pw => {
		const digits = pw.toString();

		let hasDouble = false;

		let lastDigit = parseInt(digits[0], 10);
		
		const matchingGroups = {};
		matchingGroups[lastDigit] = 1;

		for (let i = 1; i <= digits.length; i++) {
			const currentDigit = parseInt(digits[i], 10);

			if(currentDigit < lastDigit) {
				return false;
			}

			if(matchingGroups[currentDigit] === undefined) {
				matchingGroups[currentDigit] = 0;
			}

			matchingGroups[currentDigit]++;

			lastDigit = currentDigit;
		}

		return Object.values(matchingGroups).some(v => v === 2);
	};

	let possiblePws = 0;

	for (var i = range[0]; i <= range[1]; i++) {
		if(isValid(i)) {
			possiblePws++
		}
	}

	return possiblePws;
}


doIt(input);