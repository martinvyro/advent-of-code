program = "3,8,1001,8,10,8,105,1,0,0,21,30,55,76,97,114,195,276,357,438,99999,3,9,102,3,9,9,4,9,99,3,9,1002,9,3,9,1001,9,5,9,1002,9,2,9,1001,9,2,9,102,2,9,9,4,9,99,3,9,1002,9,5,9,1001,9,2,9,102,5,9,9,1001,9,4,9,4,9,99,3,9,1001,9,4,9,102,5,9,9,101,4,9,9,1002,9,4,9,4,9,99,3,9,101,2,9,9,102,4,9,9,1001,9,5,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,99,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,99";

// program = "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5";
// program = "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10";

runIt = program => {
	const options = [5, 6, 7, 8, 9];
	const sequences = [];

	for(const pos1 of options) {
		for(const pos2 of options.filter(o => o !== pos1)) {
			for(const pos3 of options.filter(o => o !== pos1 && o !== pos2)) {
				for(const pos4 of options.filter(o => o !== pos1 && o !== pos2 && o !== pos3)) {
					for(const pos5 of options.filter(o => o !== pos1 && o !== pos2 && o !== pos3 && o !== pos4)) {
						sequences.push([pos1, pos2, pos3, pos4, pos5]);
					}
				}
			}
		}
	}

	const possibleThrusterSignals = []

	for(const sequence of sequences) {
		const phase0 = intCodeComputer(program, [sequence[0]]);
		const phase1 = intCodeComputer(program, [sequence[1]]);
		const phase2 = intCodeComputer(program, [sequence[2]]);
		const phase3 = intCodeComputer(program, [sequence[3]]);
		const phase4 = intCodeComputer(program, [sequence[4]]);

		const loop = (instruction, lastLoopOutput, loopCount = 0) => {
			const outputPhase0 = phase0.run(instruction);
			const outputPhase1 = phase1.run(outputPhase0);
			const outputPhase2 = phase2.run(outputPhase1);
			const outputPhase3 = phase3.run(outputPhase2);
			const outputPhase4 = phase4.run(outputPhase3);
			if(outputPhase0.done || outputPhase1.done || outputPhase2.done || outputPhase3.done || outputPhase4.done) {
				return lastLoopOutput;
			} else {
				return loop(outputPhase4, outputPhase4, ++loopCount);
			}
		};

		const initialAmplifierInput = 0;
		const signal = loop(initialAmplifierInput, -1);

		possibleThrusterSignals.push(signal);
	}

	return Math.max(...possibleThrusterSignals);
}

// 4374895
runIt(program);
