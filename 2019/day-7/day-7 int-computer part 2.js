intCodeComputer = (programCode, inputs/* = (a, stepFn) => stepfn()*/) => {
	const program = programCode
		.split(",")
		.map(i => parseInt(i, 10));

	const outputs = [];

	let counter = 0;

	const write = (val, pos, mode) => {
		program[program[pos]] = val;
	}

	const step = (input) => {
		if(input !== undefined) {
			inputs.push(input);
		}

		const operation = program[counter].toString();

		const opCode = parseInt(operation.substr(operation.length-2), 10);

		const aMode = operation.length >= 3 ? operation.substr(operation.length-3, 1) : "0";
		const bMode = operation.length >= 4 ? operation.substr(operation.length-4, 1) : "0";

		const a = aMode === "0" ? program[program[counter+1]] : program[counter+1];
		const b = bMode === "0" ? program[program[counter+2]] : program[counter+2];

		if(opCode === 1) {
			write(a+b, counter+3);
			counter += 4;
			return step();
		} else if (opCode === 2) {
			write(a*b, counter+3);
			counter += 4;
			return step();
		} else if (opCode === 3) {
			const inputVal = inputs.shift();
			write(inputVal, counter+1, aMode);
			counter += 2;
			return step();
		} else if (opCode === 4) {
			outputs.push(a);
			counter += 2;
			return a;
		} else if (opCode === 5) { // jump if true
			if(a !== 0) {
				counter = b;
			} else {
				counter +=3;
			}
			return step();
		} else if (opCode === 6) { // jump if false
			if(a === 0) {
				counter = b;
			} else {
				counter += 3;
			}
			return step();
		} else if (opCode === 7) { // less than
			write(a < b ? 1 : 0, counter+3);
			counter += 4;
			return step();
		} else if (opCode === 8) { // equals
			write(a === b ? 1 : 0, counter+3);
			counter += 4;
			return step();
		} else if (opCode === 99) {
			return { done: true };
		} else {
			console.warn(`unknown instruction: ${opcode}`);
			return `unknown instruction: ${opcode}`;
		}
	};

	return {
		run: step
	}
}