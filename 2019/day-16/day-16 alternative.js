input = "59769638638635227792873839600619296161830243411826562620803755357641409702942441381982799297881659288888243793321154293102743325904757198668820213885307612900972273311499185929901117664387559657706110034992786489002400852438961738219627639830515185618184324995881914532256988843436511730932141380017180796681870256240757580454505096230610520430997536145341074585637105456401238209187118397046373589766408080120984817035699228422366952628344235542849850709181363703172334788744537357607446322903743644673800140770982283290068502972397970799328249132774293609700245065522290562319955768092155530250003587007804302344866598232236645453817273744027537630";
phases = 100;

fft = (input, phases) => {
	const messageOffset = parseInt(input.substr(0, 7), 10);

	const digits = [];
	for(const digit of input) {
		digits.push(parseInt(digit, 10));
	}

	const k = 10000;
	const basePattern = [0, 1, 0, -1];


	const getNextIndex = patternValIndex => {
		// console.log("get next index", patternValIndex)
		if(patternValIndex >= basePattern.length - 1) {
			return 0;
		} else {
			return ++patternValIndex;
		}
	};


	let prevPhase = digits;
	let phaseResults = [];

	const digitsLen = prevPhase.length;

	for(let phase = 1; phase <= phases; phase++) {
		for(let row = 0; row < digitsLen; row++) {
			// console.log(`row ${row}`);
			let patternValIndex = 0;
			let patternVal = basePattern[patternValIndex];

			let sum = 0;
			for(let col = 0; col < digitsLen; col++) {
				if((col + 1) % (row + 1) === 0) {
					patternValIndex = getNextIndex(patternValIndex);
					patternVal = basePattern[patternValIndex];
				}

				sum += prevPhase[col % digitsLen] * patternVal;
			}

			const res = sum.toString();
			const rowSum = Math.abs(parseInt(res.substr(res.length - 1), 10));
			phaseResults.push(rowSum);
		}

		prevPhase = phaseResults;
		phaseResults = [];
	}

	return prevPhase.join("").substr(0, 8);
}

fft(input, phases);
