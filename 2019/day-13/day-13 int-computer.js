intCodeComputer = (programCode, inputs, askForInput) => {
	const program = programCode
		.split(",")
		.map(i => parseInt(i, 10));

	const outputs = [];

	let counter = 0;
	let relativeBaseCounter = 0;

	const write = (val, pos) => {
		program[pos] = val;
	};

	let i = 0;

	const step = input => {
		if(input !== undefined) {
			inputs.push(input);
		}

		while(i<1000000) {
			const operation = program[counter].toString();
			const opCode = parseInt(operation.substr(operation.length-2), 10);

			const aMode = operation.length >= 3 ? operation.substr(operation.length-3, 1) : "0";
			const bMode = operation.length >= 4 ? operation.substr(operation.length-4, 1) : "0";
			const cMode = operation.length >= 5 ? operation.substr(operation.length-5, 1) : "0";

			const a = (aMode === "0"
				? program[program[counter+1]]
				: aMode === "1"
					? program[counter+1]
					: program[relativeBaseCounter + program[counter+1]])
				|| 0;

			const aPointer = aMode === "0"
				? program[counter+1]
				: aMode === "1"
					? counter+1
					: relativeBaseCounter + program[counter+1];

			const b = (bMode === "0"
				? program[program[counter+2]]
				: bMode === "1"
					? program[counter+2]
					: program[relativeBaseCounter + program[counter+2]])
				|| 0;

			const cPointer = cMode === "0"
				? program[counter+3]
				: cMode === "1"
					? counter+3
					: relativeBaseCounter + program[counter+3];

			if(opCode === 1) { // add
				write(a+b, cPointer, aMode);
				counter += 4;
			} else if (opCode === 2) { // multiply
				write(a*b, cPointer, aMode);
				counter += 4;
			} else if (opCode === 3) { // read input
				const inputVal = askForInput();
				// console.log( `read input ${inputVal}` );
				write(inputVal, aPointer);
				counter += 2;
			} else if (opCode === 4) { // output
				outputs.push(a);
				counter += 2;
				return a;
			} else if (opCode === 5) { // jump if true
				if(a !== 0) {
					counter = b;
				} else {
					counter +=3;
				}
			} else if (opCode === 6) { // jump if false
				if(a === 0) {
					counter = b;
				} else {
					counter += 3;
				}
			} else if (opCode === 7) { // less than
				write(a < b ? 1 : 0, cPointer);
				counter += 4;
			} else if (opCode === 8) { // equals
				write(a === b ? 1 : 0, cPointer);
				counter += 4;
			} else if (opCode === 9) { // adjust relative base
				relativeBaseCounter += a;
				counter += 2;
			} else if (opCode === 99) {
				// halt
				console.log( 'halt', outputs );
				return { done: true };
			} else {
				console.warn('unknown instruction', opCode)
				return;
			}
			i++;
		}
		console.log("timed out:", program, outputs);
	};

	return {
		run: step
	};
}