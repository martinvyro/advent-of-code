positions = [{x:5, y:-1, z:5}, {x:0, y:-14, z:2}, {x:16, y:4, z:0}, {x:18, y:1, z:16}];

// positions = [{x:-1, y:0, z:2}, {x:2, y:-10, z:-7}, {x:4, y:-8, z:8}, {x:3, y:5, z:-1}];
// positions = [{x:-8, y:-10, z:0},{x:5, y:5, z:10},{x:2, y:-7, z:3},{x:9, y:-8, z:-3}];

runIt = positions => {
	const steps = [positions.map(pos => ({ pos, vel: { x: 0, y: 0, z: 0} }))];

	const timeSteps = 1000;
	// apply gravity
		// consider every pair of moons,
		// 
	// apply velocity
		//
	const getGravityChange = (a, b) => {
		return a > b
			? -1
			: a < b
				? 1
				: 0;
	};

	const applyGravity = (moon, otherMoons) => {
		let { x, y, z } = moon.vel; // initial velocity
		const { pos } = moon;
		otherMoons.forEach(otherMoon => {
			x += getGravityChange(pos.x, otherMoon.pos.x);
			y += getGravityChange(pos.y, otherMoon.pos.y);
			z += getGravityChange(pos.z, otherMoon.pos.z);
		});
		return { x, y, z };
	};

	const applyVelocity = (pos, vel) => ({
		x: pos.x + vel.x,
		y: pos.y + vel.y,
		z: pos.z + vel.z
	});

	const simulateMotion = moons => {
		return moons.map(moon => {
			const vel = applyGravity(moon, moons.filter(m => m !== moon));
			const pos = applyVelocity(moon.pos, vel);
			return { pos, vel };
		});
	};

	for(let step = 1; step <= timeSteps; step++) {
		steps.push(simulateMotion(steps[step-1]))
	}

	// calculate total energy in the system
		// potential energy * kinetic energy
		// potential energy = sum of the absolute values of x,y,z (pos)
		// kinetic energy = sum of the absolute values of its velocity (x, y, z)

	const getMoonsEnergy = values => {
		const { x, y, z } = values;
		return Math.abs(x) + Math.abs(y) + Math.abs(z)
	};

	const moons = steps[timeSteps];

	const totalEnergy = moons.map(moon => getMoonsEnergy(moon.pos) * getMoonsEnergy(moon.vel)).reduce((cur, prev) => cur + prev, 0);

	console.log(totalEnergy);

	return steps;
}

runIt(positions);
