positions = [{x:5, y:-1, z:5}, {x:0, y:-14, z:2}, {x:16, y:4, z:0}, {x:18, y:1, z:16}];

// positions = [{x:-1, y:0, z:2}, {x:2, y:-10, z:-7}, {x:4, y:-8, z:8}, {x:3, y:5, z:-1}];
// positions = [{x:-8, y:-10, z:0},{x:5, y:5, z:10},{x:2, y:-7, z:3},{x:9, y:-8, z:-3}];

runIt = positions => {
	const initialStep = positions.map(pos => ({ pos, vel: { x: 0, y: 0, z: 0} }));

	const timeSteps = 1000;

	const getGravityChange = (a, b) => {
		return a > b
			? -1
			: a < b
				? 1
				: 0;
	};

	const applyGravity = (moon, otherMoons, axis) => {
		let velocity = moon.vel[axis];
		const { pos } = moon;
		otherMoons.forEach(otherMoon => {
			velocity += getGravityChange(pos[axis], otherMoon.pos[axis]);
		});
		return velocity;
	};

	const simulateMotion = (moons, axis) => {
		return moons.map(moon => {
			const vel = applyGravity(moon, moons.filter(m => m !== moon), axis);
			const pos = moon.pos[axis] + vel;
			const velocity = {};
			velocity[axis] = vel;
			const position = {};
			position[axis] = pos;
			return { pos: position, vel: velocity };
		});
	};

	const positionIsEqual = (a, b, axis) => {
		const moonAAxisPos = a.map(m => m.pos[axis]);
		const moonBAxisPos = b.map(m => m.pos[axis]);
		return moonAAxisPos.every((p, i) => moonBAxisPos[i] === p);
	};

	const findRepeatingPosition = (axis, initialStep) => {
		let i = 0;
		let lastStep = initialStep;

		// this is probably the first "valid" use of do .. while in my career
		do {
			lastStep = simulateMotion(lastStep, axis);
			i++;
		} while(!positionIsEqual(lastStep, initialStep, axis));

		return i + 1;
	};

	const stepsTilXRepeats = findRepeatingPosition("x", initialStep);
	const stepsTilYRepeats = findRepeatingPosition("y", initialStep);
	const stepsTilZRepeats = findRepeatingPosition("z", initialStep);

	// 186028 231614 96236
	console.log( stepsTilXRepeats, stepsTilYRepeats, stepsTilZRepeats );
	// result is Least Common Multiple of the above
};

runIt(positions);

// 345 527 005 447 284 too low
// 518 311 327 635 164 ✔️
// 4 146 324 065 367 408 too high
// 4 146 407 342 710 485 too high